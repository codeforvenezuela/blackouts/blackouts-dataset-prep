# Blackouts Dataset Prep

This repo aims to create the training dataset for the Blackouts Project.

 ## [About the Data](https://gitlab.com/codeforvenezuela/blackouts/blackouts-dataset-prep/-/blob/master/tagging-set-original_for_jupyter_tagging.csv)

The untagged dataset originates from scraped tweets by Code For Venezuela's Angostura ETL. A subset of tweets (11,000) was queried from the etl in order for them to be tagged. The first 4,000 tweets have already been tagged. You can find three types of tags: 

- label_country: Country where the user reports a problem with public services. 
- label_state: Country's state where the user reports a problem with public service. 
- label_type: Type of public service problem. E.g. #SinLuz, #SinGasolina, #SinAgua etc...

## [About the Notebook](https://gitlab.com/codeforvenezuela/blackouts/blackouts-dataset-prep/-/blob/master/twitter_pretagging.ipynb)

In this notebook can serve as template to start exploring and tagging the labels. The purpose of the notebook is to create a reproducible way to manually tag tweets. This notebook is not intended to replace the machine learning model where the automated tagging is going to be performed.

### How to run the notebook in colab

1. Click on the *Open in Colab* link at the top of the notebook. 
2. Download [tagging-set-original_for_jupyter_tagging.csv](https://gitlab.com/codeforvenezuela/blackouts/blackouts-dataset-prep/-/blob/master/tagging-set-original_for_jupyter_tagging.csv)
3. Upload dataset to google colab

**Note:** It is recommended to clone the repo, create a new branch, and work locally within the new branch. 
